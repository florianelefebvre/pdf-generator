import { Row } from "redux/reducers/table.reducers";

export const swap = (array: Row[], x: number, y: number): Row[] => {
  const tempObj = array[x];
  array[x] = array[y];
  array[y] = tempObj;
  return array;
};
