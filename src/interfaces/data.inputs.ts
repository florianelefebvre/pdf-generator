export interface DataInput {
  placeholder: string;
  key: string;
  value?: string;
  label?: string;
}
