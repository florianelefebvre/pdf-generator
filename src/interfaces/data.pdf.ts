import { DataInput } from "interfaces/data.inputs";
import { Row } from "redux/reducers/table.reducers";

export interface pdfDataObj {
  header: {
    customerInputsData: DataInput[];
    companyInputsData: DataInput[];
    invoiceInputsData: DataInput[];
  };
  globalDiscount: boolean;
  deposit: boolean;
  totalRows: {
    label: string;
    value: string;
  }[];
  totalVatRows: {
    label: string;
    value: string;
  }[];
  rows: Row[];
}
