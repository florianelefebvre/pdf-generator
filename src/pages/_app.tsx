import "../styles/globals.css";
import type { AppProps } from "next/app";

import { GeistProvider, CssBaseline } from "@geist-ui/core";
import { wrapper } from "redux/store";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <GeistProvider>
      <CssBaseline />
      <Component {...pageProps} />
    </GeistProvider>
  );
}

export default wrapper.withRedux(MyApp);
