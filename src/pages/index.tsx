import { SetStateAction, useEffect, useState } from "react";
import type { NextPage } from "next";
import styled from "styled-components";
import { BlobProvider, PDFDownloadLink, PDFViewer } from "@react-pdf/renderer";
import { Dispatch } from "redux";

// redux
import { useTypedSelector } from "hooks/useTypeSelector";
import { StateToUpdate, updateInput } from "redux/actions/inputs.actions";

import { useDispatch } from "react-redux";

// components
import PdfDocument from "../components/pdf/Document.pdf";
import Image from "next/image";
import { Button, Page, Text, Input } from "@geist-ui/core";
import TableFullRowInputs from "components/inputs/TableFullRow.inputs";

// interfaces
import { DataInput } from "interfaces/data.inputs";

// icons
import { Plus } from "@styled-icons/evaicons-solid";
import { addBlankRow } from "redux/actions/table.actions";
import TableTotalsRow from "components/inputs/TableTotalsRow.inputs";

const Home: NextPage = () => {
  const [showPdfLink, setShowPdfLink] = useState(false);

  const {
    customer: customerInputsData,
    invoice: invoiceInputsData,
    company: companyInputsData,
  } = useTypedSelector((state) => state.inputs);

  const { headings, rows, totalRows, totalVatRows, deposit } = useTypedSelector(
    (state) => state.table
  );

  useEffect(() => {
    if (typeof window !== undefined) {
      setShowPdfLink(true);
    }
  }, []);

  const dispatch = useDispatch();

  const displayInputs = (
    inputsArray: DataInput[],
    stateToUpdate: StateToUpdate,
    showLabel: boolean
  ) => {
    const inputsList = inputsArray.map((input, index) => {
      return (
        <FlexRow key={index}>
          {showLabel && (
            <Text font="12px" style={{ margin: 0 }}>
              {input.label}
            </Text>
          )}

          <Input
            height={0.6}
            width={13}
            margin={0.2}
            placeholder={input.placeholder}
            value={input.value}
            onChange={(event) => {
              dispatch(
                updateInput({
                  index,
                  stateToUpdate,
                  eventTargetValue: event.target.value,
                })
              );
            }}
          />
        </FlexRow>
      );
    });
    return inputsList;
  };

  const document = (
    <PdfDocument
      data={{
        header: { companyInputsData, customerInputsData, invoiceInputsData },
        globalDiscount: true,
        deposit: true,
        totalRows,
        rows,
        totalVatRows,
      }}
    />
  );

  return (
    <Page>
      <Text h3>Invoice PDF generator</Text>

      <DocumentEntryies>
        <Header>
          <LeftHeader>
            <Logo>
              {/* eslint-disable-next-line jsx-a11y/alt-text */}
              <Image src={"/logo_big_black.png"} width={20} height="20px" />
              <Text style={{ marginLeft: "10px" }}>USER AGENCY</Text>
            </Logo>

            {displayInputs(companyInputsData, "company", false)}
          </LeftHeader>
          <RightHeader>
            <InvoiceAndDate>
              {displayInputs(
                [invoiceInputsData[0], invoiceInputsData[1]],
                "invoice",
                true
              )}
            </InvoiceAndDate>
            <ClientDetails>
              {displayInputs(customerInputsData, "customer", false)}
            </ClientDetails>
            <InvoiceTitle>
              <Input
                scale={2}
                height={0.6}
                width={"100%"}
                margin={0.2}
                placeholder={invoiceInputsData[2].placeholder}
                onChange={(event) => {
                  dispatch(
                    updateInput({
                      index: 2,
                      stateToUpdate: "invoice",
                      eventTargetValue: event.target.value,
                    })
                  );
                }}
              />
            </InvoiceTitle>
          </RightHeader>
        </Header>

        <Body>
          <DeliveryDate>
            {displayInputs([invoiceInputsData[3]], "invoice", true)}
          </DeliveryDate>

          <Table>
            <TableFullRowInputs row={headings} rowHeading={true} index={0} />
            {rows.map((row, index) => {
              return (
                <TableFullRowInputs
                  key={index}
                  index={index}
                  row={row}
                  rowHeading={false}
                  lastRow={rows.length - 1 === index}
                />
              );
            })}

            <TableTotalsRow row={totalRows[0]} />
            {totalVatRows.map((row, index) => {
              return <TableTotalsRow key={index} row={row} />;
            })}

            {totalRows.map((row, index) => {
              if (row.label === "Acompte" && !deposit) return;
              if (index > 0) return <TableTotalsRow key={index} row={row} />;
            })}
          </Table>

          <FlexRow>
            <Plus
              color="green"
              size="20px"
              onClick={() => dispatch(addBlankRow({ rowIndex: rows.length }))}
              style={{ cursor: "pointer" }}
            />
            <Text font="12px">Ajouter une ligne </Text>
          </FlexRow>
        </Body>
      </DocumentEntryies>

      {showPdfLink && (
        <>
          <PDFDownloadLink
            document={document}
            fileName={"INVOICE_" + invoiceInputsData[0].value}
          >
            {({ blob, url, loading, error }) =>
              loading ? (
                <Button>Chargement du document...</Button>
              ) : (
                <Button>Générer PDF</Button>
              )
            }
          </PDFDownloadLink>
          {/* <PDFViewer>
            <PdfDocument data="Voici la data" />
          </PDFViewer> */}

          <BlobProvider document={document}>
            {({ blob, url, loading, error }) => {
              if (blob) {
                const url = URL.createObjectURL(blob);
                return (
                  <a href={url} style={{ marginLeft: "20px" }}>
                    Voir le PDF
                  </a>
                );
              }
              if (error) return error;
              return <div>The PDF is rendering...</div>;
            }}
          </BlobProvider>
        </>
      )}
    </Page>
  );
};

export default Home;

const DocumentEntryies = styled.div`
  border: solid lightGrey 1px;
  max-width: 827px;
  min-width: 600px;
  padding: 20px;
  margin-bottom: 20px;
`;

const Logo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const LeftHeader = styled.div`
  display: flex;
  flex-direction: column;
`;

const RightHeader = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 45%;
`;

const InvoiceAndDate = styled.div`
  border-left: solid black 1px;
  padding: 5px 15px;
`;

const ClientDetails = styled.div`
  margin-top: 30px;
  border: solid black 1px;
  border-radius: 5px;
  padding: 5px 15px;
`;

const InvoiceTitle = styled.div`
  margin: 20px 0 0 -100px;
`;

const Body = styled.div`
  margin: 20px 0;
`;

const DeliveryDate = styled.div``;

const Table = styled.div``;
