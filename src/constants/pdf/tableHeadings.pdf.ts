export const tableHeadings = {
  subtotal: false,
  subtitle: false,
  columns: [
    { key: "reference", value: "Référence" },
    {
      key: "name",
      value: "Désignation",
    },
    { key: "quantity", value: "Quantité" },
    { key: "pricePerUnitET", value: "Prix U HT" },
    { key: "subtotalPriceET", value: "Montant HT" },
    { key: "vat", value: "TVA %" },
  ],
};
