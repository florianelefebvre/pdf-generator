export const companyInputs = [
  {
    placeholder: "Adresse",
    key: "address",
    value: "165, avenue de Bretagne",
    label: "",
  },
  {
    placeholder: "Code postal",
    key: "zipCode",
    value: "59 000",
  },
  {
    placeholder: "Ville",
    key: "cityName",
    value: "LILLE",
  },
  {
    placeholder: "Pays",
    key: "countryName",
    value: "France",
  },
  {
    placeholder: "Téléphone",
    key: "phoneNumber",
    value: "",
  },
  {
    placeholder: "Email",
    key: "email",
    value: "",
  },
  {
    placeholder: "SIRET",
    key: "siretNumber",
    value: "83782569400024",
    label: "N° SIRET :",
  },
  {
    placeholder: "RCS Ville",
    key: "cityRcsNumber",
    value: "Lille Metropole B 837825694",
    label: "RCS :",
  },
  {
    placeholder: "Code NAF",
    key: "nafCode",
    value: "6202A",
    label: "Code NAF :",
  },
  {
    placeholder: "Numéro de TVA",
    key: "vatNumber",
    value: "FR96837825694",
    label: "N°TVA :",
  },
];
