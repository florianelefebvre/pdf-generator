export const tableBlankRowInputs = {
  subtotal: false,
  subtitle: false,

  columns: [
    {
      key: "reference",
      placeholder: "Référence",
      value: "",
    },
    {
      key: "name",
      placeholder: "Entrer ici le nom du produit",
      value: "",
      subtitle: {
        placeholder: "Entrer ici un sous-titre",
        value: "",
      },
      subtotal: {
        value: "Sous-total",
      },
    },
    {
      key: "quantity",
      value: "0",
    },
    {
      key: "pricePerUnitET",
      value: "0.00",
    },
    {
      key: "discountPercent",
      value: "0.00",
    },
    {
      key: "subtotalPriceET",
      value: "0.00",
      subtotal: {
        value: "0.00",
      },
    },
    {
      key: "vat",
      value: "20.00",
    },
  ],
};
