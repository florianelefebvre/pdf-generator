export const customerInputs = [
  {
    placeholder: "Facultatif: code client",
    key: "id",
    label: "Code client :",
    value: "",
  },
  {
    placeholder: "Nom et prénom du client",
    key: "fullName",
    value: "",
  },
  {
    placeholder: "Adresse",
    key: "address",
    value: "",
  },
  {
    placeholder: "Code postal",
    key: "zipCode",
    value: "",
  },
  {
    placeholder: "Ville",
    key: "cityName",
    value: "",
  },
  {
    placeholder: "Téléphone",
    key: "phoneNumber",
    value: "",
  },
  {
    placeholder: "Facultatif : Numéro de TVA du client",
    key: "vatNumber",
    label: "N°TVA :",
    value: "",
  },
];
