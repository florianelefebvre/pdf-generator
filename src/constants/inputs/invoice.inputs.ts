export const invoiceInputs = [
  {
    placeholder: "",
    key: "number",
    label: "FACTURE N°",
    value: "",
  },
  {
    placeholder: "",
    key: "date",
    label: "Date d'émission :",
    value: new Date().toLocaleString("fr-FR").split(",")[0],
  },
  {
    placeholder: "Mettez ici un titre",
    key: "invoiceTitle",
    value: "",
  },
  {
    placeholder: "",
    key: "deliveryDate",
    value: new Date().toLocaleString("fr-FR").split(",")[0],
    label: "Date d'exécution de la vente ou de la prestation :",
  },
];
