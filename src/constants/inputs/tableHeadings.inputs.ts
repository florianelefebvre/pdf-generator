export const tableHeadingsInputs = {
  subtotal: false,
  subtitle: false,
  columns: [
    {
      key: "reference",
      value: "Référence",
    },
    {
      key: "name",
      value: "Désignation",
    },
    {
      key: "quantity",
      value: "Quantité",
    },
    // {
    //   key: "test",
    //   value: "test",
    // },
    {
      key: "pricePerUnitET",
      value: "Prix Unitaire HT",
    },

    {
      key: "discountPercent",
      value: "Remise %",
    },
    {
      key: "subtotalPriceET",
      value: "Montant HT",
    },
    {
      key: "vat",
      value: "TVA %",
    },
  ],
};
