// export const fakeTableObj = {
//   globalDiscount: true,
//   deposit: true,
//   totalRows: [
//     { name: "Total HT", value: "350" },
//     {
//       name: "Remise globale 10%",
//       value: "100",
//     },
//     { name: "Total HT après remise globale", value: "270" },
//     { name: "Total TTC", value: "270" },
//     { name: "Acompte", value: "-10" },
//     { name: "Net à Payer (€)", value: "270" },
//   ],

//   rows: [
//     {
//       subtotal: false,
//       subtitle: true,
//       columns: [
//         { key: "reference", value: "cho13456" },
//         {
//           key: "name",
//           value: "chocolate",
//           subtitle: "voici le sous titre",
//         },
//         { key: "quantity", value: "10" },
//         { key: "pricePerUnitET", value: "5" },
//         { key: "subtotalPriceET", value: "50", subtotal: "300" },
//         { key: "vat", value: "0" },
//       ],
//     },
//     {
//       subtotal: true,
//       subtitle: false,
//       columns: [
//         { key: "reference", value: "cho13456" },
//         {
//           key: "name",
//           value: "chocolate",
//           subtitle: "voici le sous titre",
//         },
//         { key: "quantity", value: "10" },
//         { key: "pricePerUnitET", value: "5" },
//         { key: "subtotalPriceET", value: "50", subtotal: "300" },
//         { key: "vat", value: "0" },
//       ],
//     },
//     {
//       subtotal: false,
//       subtitle: false,
//       columns: [
//         { key: "reference", value: "cho13456" },
//         {
//           key: "name",
//           value: "chocolate",
//           subtitle: "voici le sous titre",
//         },
//         { key: "quantity", value: "10" },
//         { key: "pricePerUnitET", value: "5" },
//         { key: "subtotalPriceET", value: "50", subtotal: "300" },
//         { key: "vat", value: "0" },
//       ],
//     },
//     {
//       subtotal: true,
//       subtitle: true,
//       columns: [
//         { key: "reference", value: "cho13456" },
//         {
//           key: "name",
//           value: "chocolate",
//           subtitle: "voici le sous titre",
//         },
//         { key: "quantity", value: "10" },
//         { key: "pricePerUnitET", value: "5" },
//         { key: "subtotalPriceET", value: "50", subtotal: "300" },
//         { key: "vat", value: "0" },
//       ],
//     },
//   ],
// };
