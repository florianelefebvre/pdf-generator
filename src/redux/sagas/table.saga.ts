import { call, takeLatest, put, select } from "redux-saga/effects";
import {
  UpdateRow,
  TypesTable,
  updateInputTable,
  setCheckbox,
  UpdateCheckboxes,
  UpdateTableOrder,
  swapRow,
  updateTotals,
  updateVatTotalsState,
  UpdateDeposit,
} from "redux/actions/table.actions";
import { Row } from "redux/reducers/table.reducers";

const calcTotalRow = (row: Row) => {
  const quantity = Number(row.columns[2].value);
  const pricePerUnit = Number(row.columns[3].value);
  const discountPercent = Number(row.columns[4].value);
  return (1 - discountPercent / 100) * quantity * pricePerUnit;
};

function* updateTotalRow(rowIndex: number) {
  const rows: Row[] = yield select((state) => state.table.rows);
  const totalRowValue = calcTotalRow(rows[rowIndex]);
  yield put(
    updateInputTable({
      rowIndex,
      columnIndex: 5,
      eventTargetValue: totalRowValue.toFixed(2),
      inputType: "value",
    })
  );
}

function* updateSubtotalsTable() {
  const rows: Row[] = yield select((state) => state.table.rows);
  let sum = 0;
  for (let i = 0; i < rows.length; i++) {
    if (rows[i].subtotal) {
      sum += Number(rows[i].columns[5].value);
      yield put(
        updateInputTable({
          rowIndex: i,
          columnIndex: 5,
          eventTargetValue: sum.toFixed(2),
          inputType: "subtotal",
        })
      );
      sum = 0;
    } else {
      sum += Number(rows[i].columns[5].value);
    }
  }
}

function* updateTotalET() {
  const rows: Row[] = yield select((state) => state.table.rows);
  const initialValue = 0;
  const sum = rows.reduce(
    (prev, curr) => prev + Number(curr.columns[5].value),
    initialValue
  );
  yield put(updateTotals({ rowIndex: 0, value: sum.toFixed(2) }));
}

function* updateTotalVAT() {
  const rows: Row[] = yield select((state) => state.table.rows);
  // create data objects
  const vatData = rows.map((row) => {
    const totalAmount =
      (Number(row.columns[6].value) / 100) * Number(row.columns[5].value);
    return { label: `total tva ${row.columns[6].value} %`, totalAmount };
  });
  // filter by vat %
  const filteredVat = vatData.filter(
    (value, index, self) =>
      self.findIndex((v) => v.label === value.label) === index
  );
  // reduce amounts per vat %
  const reducedVat = filteredVat.map((vat) => {
    const allSameVatTab = vatData.filter((obj) => obj.label === vat.label);
    const totalAMount = allSameVatTab.reduce(
      (total, elem) => total + Number(elem.totalAmount),
      0
    );
    return { label: vat.label, value: totalAMount.toFixed(2) };
  });

  // update state
  yield put(updateVatTotalsState({ vatTotalRows: reducedVat }));
}
function* updateTotalIT() {
  const { totalRows, totalVatRows } = yield select((state) => state.table);
  const totalVat = totalVatRows.reduce(
    (total: number, obj: { label: string; value: string }) =>
      total + Number(obj.value),
    0
  );
  const totalIT = totalVat + Number(totalRows[0].value);
  yield put(updateTotals({ rowIndex: 1, value: totalIT.toFixed(2) }));
}

function* updateTotalToPay() {
  const { totalRows } = yield select((state) => state.table);
  const totalToPay = Number(totalRows[1].value) - Number(totalRows[2].value);
  yield put(updateTotals({ rowIndex: 3, value: totalToPay.toFixed(2) }));
}

function* updateTotalsTable() {
  yield call(updateTotalET);
  yield call(updateTotalVAT);
  yield call(updateTotalIT);
  yield call(updateTotalToPay);
}

function* updateRowSaga(action: UpdateRow) {
  yield put(updateInputTable(action.payload));
  const { columnIndex, rowIndex } = action.payload;
  // if input is dependency
  if (
    columnIndex === 2 ||
    columnIndex === 3 ||
    columnIndex === 4 ||
    columnIndex === 6
  ) {
    yield call(updateTotalRow, rowIndex);
    yield call(updateSubtotalsTable);
    yield call(updateTotalsTable);
  }
}

function* updateCheckboxesSaga(action: UpdateCheckboxes) {
  yield put(setCheckbox(action.payload));
  const { checkBoxId } = action.payload;
  if (checkBoxId === "=") {
    yield call(updateSubtotalsTable);
  }
}

function* updateTableOrderSaga(action: UpdateTableOrder) {
  yield put(swapRow(action.payload));
  yield call(updateSubtotalsTable);
}

function* updateDepositSaga(action: UpdateDeposit) {
  yield put(updateTotals(action.payload));
  yield call(updateTotalToPay);
}

export function* watchTableSaga() {
  yield takeLatest(TypesTable.UPDATE_ROW, updateRowSaga);
  yield takeLatest(TypesTable.UPDATE_CHECKBOXES, updateCheckboxesSaga);
  yield takeLatest(TypesTable.UPDATE_TABLE_ORDER, updateTableOrderSaga);
  yield takeLatest(TypesTable.UPDATE_DEPOSIT, updateDepositSaga);
}
