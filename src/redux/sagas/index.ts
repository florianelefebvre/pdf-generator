import { all, fork } from "redux-saga/effects";
import { watchTableSaga } from "./table.saga";

// fonction qui écoute tous les workers des sagas
// déclenchée au .run() dans store

function* rootSaga() {
  yield all([fork(watchTableSaga)]);
}

export default rootSaga;
