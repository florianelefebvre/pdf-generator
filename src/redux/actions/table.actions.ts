export enum TypesTable {
  UPDATE_ROW = "table/UPDATE_ROW",
  UPDATE_INPUT_TABLE = "table/UPDATE_INPUT_TABLE",
  SET_TOTAL_ROW = "table/SET_TOTAL_ROW",
  SET_CHECKBOX = "table/SET_CHECKBOXE",
  SWAP_ROW = "table/SWAP_ROW",
  ADD_BLANK_ROW = "table/ADD_BLANK_ROW",
  DELETE_ROW = "table/DELETE_ROW",
  UPDATE_CHECKBOXES = "table/UPDATE_CHECKBOXES",
  SET_SUBTOTALS_ROW = "table/SET_SUBTOTALS_ROW",
  UPDATE_TABLE_ORDER = "table/UPDATE_TABLE_ORDER",

  // totals
  UPDATE_TOTALS = "table/UPDATE_TOTALS",
  UPDATE_VAT_TOTALS = "table/UPDATE_VAT_TOTALS",
  UPDATE_DEPOSIT = "table/UPDATE_DEPOSIT",
}
type InputType = "value" | "subtitle" | "subtotal";

interface UpdateRowPayload {
  rowIndex: number;
  columnIndex: number;
  eventTargetValue: string;
  inputType: InputType;
}

interface RowIndex {
  rowIndex: number;
}

interface UpdateCheckboxesPayload {
  rowIndex: number;
  checkBoxId: string;
  eventTargetValue: boolean;
}

interface SwapRowPayload {
  direction: string;
  rowIndex: number;
}

interface UpdateTotalsPayload {
  rowIndex: number;
  value: string;
}

interface UpdateVatTotalsStatePayload {
  vatTotalRows: {
    label: string;
    value: string;
  }[];
}

// types actions

export interface UpdateRow {
  type: TypesTable.UPDATE_ROW;
  payload: UpdateRowPayload;
}

interface UpdateInputTable {
  type: TypesTable.UPDATE_INPUT_TABLE;
  payload: UpdateRowPayload;
}

export interface SetTotalRow {
  type: TypesTable.SET_TOTAL_ROW;
  payload: RowIndex;
}

export interface setCheckbox {
  type: TypesTable.SET_CHECKBOX;
  payload: UpdateCheckboxesPayload;
}

export interface SwapRow {
  type: TypesTable.SWAP_ROW;
  payload: SwapRowPayload;
}
export interface DeleteRow {
  type: TypesTable.DELETE_ROW;
  payload: RowIndex;
}

export interface AddBlankRow {
  type: TypesTable.ADD_BLANK_ROW;
  payload: RowIndex;
}

export interface SetSubtotalRow {
  type: TypesTable.SET_SUBTOTALS_ROW;
  payload: RowIndex;
}

export interface UpdateCheckboxes {
  type: TypesTable.UPDATE_CHECKBOXES;
  payload: UpdateCheckboxesPayload;
}

export interface UpdateTableOrder {
  type: TypesTable.UPDATE_TABLE_ORDER;
  payload: SwapRowPayload;
}

// update totals
export interface UpdateTotals {
  type: TypesTable.UPDATE_TOTALS;
  payload: UpdateTotalsPayload;
}

export interface UpdateVatTotalsState {
  type: TypesTable.UPDATE_VAT_TOTALS;
  payload: UpdateVatTotalsStatePayload;
}

export interface UpdateDeposit {
  type: TypesTable.UPDATE_DEPOSIT;
  payload: UpdateTotalsPayload;
}

// type Action

export type TableAction =
  // update inputs
  | UpdateRow
  | UpdateInputTable
  | SetTotalRow
  | SetSubtotalRow

  // checkboxes
  | setCheckbox
  | UpdateCheckboxes

  // actions on rows
  | UpdateTableOrder
  | SwapRow
  | DeleteRow
  | AddBlankRow

  // totals
  | UpdateTotals
  | UpdateVatTotalsState
  | UpdateDeposit;

// **** update inputs ********************

//  *** saga
export const updateRow = (payload: UpdateRowPayload) => {
  return {
    type: TypesTable.UPDATE_ROW,
    payload,
  };
}; // *** saga

export const updateInputTable = (payload: UpdateRowPayload) => {
  return {
    type: TypesTable.UPDATE_INPUT_TABLE,
    payload,
  };
};

export const setTotalRow = (payload: RowIndex) => {
  return {
    type: TypesTable.SET_TOTAL_ROW,
    payload,
  };
};

export const setSubtotalsRow = (payload: RowIndex) => {
  return {
    type: TypesTable.SET_SUBTOTALS_ROW,
    payload,
  };
};

// **** checkboxes ********************

//  *** saga
export const updateCheckboxes = (payload: UpdateCheckboxesPayload) => {
  return {
    type: TypesTable.UPDATE_CHECKBOXES,
    payload,
  };
}; //  *** saga

export const setCheckbox = (payload: UpdateCheckboxesPayload) => {
  return {
    type: TypesTable.SET_CHECKBOX,
    payload,
  };
};

// **** actions on rows ********************

//  *** saga
export const UpdateTableOrder = (payload: SwapRowPayload) => {
  return {
    type: TypesTable.UPDATE_TABLE_ORDER,
    payload,
  };
}; //  *** saga

export const swapRow = (payload: SwapRowPayload) => {
  return {
    type: TypesTable.SWAP_ROW,
    payload,
  };
};

export const deleteRow = (payload: RowIndex) => {
  return {
    type: TypesTable.DELETE_ROW,
    payload,
  };
};

export const addBlankRow = (payload: RowIndex) => {
  return {
    type: TypesTable.ADD_BLANK_ROW,
    payload,
  };
};

// **** update totals ********************

export const updateTotals = (payload: UpdateTotalsPayload) => {
  return {
    type: TypesTable.UPDATE_TOTALS,
    payload,
  };
};

export const updateVatTotalsState = (payload: UpdateVatTotalsStatePayload) => {
  return {
    type: TypesTable.UPDATE_VAT_TOTALS,
    payload,
  };
};

export const updateDeposit = (payload: UpdateTotalsPayload) => {
  return {
    type: TypesTable.UPDATE_DEPOSIT,
    payload,
  };
};
