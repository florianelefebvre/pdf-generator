export enum Types {
  UPDATE_INPUT = "inputs/UPDATE_INPUT",
}

export type StateToUpdate = "customer" | "invoice" | "company";

interface UpdateInputPayload {
  eventTargetValue: string;
  index: number;
  stateToUpdate: StateToUpdate;
}

interface UpdateInputAction {
  type: Types.UPDATE_INPUT;
  payload: UpdateInputPayload;
}
export type Action = UpdateInputAction;

export const updateInput = (payload: UpdateInputPayload) => {
  return {
    type: Types.UPDATE_INPUT,
    payload,
  };
};
