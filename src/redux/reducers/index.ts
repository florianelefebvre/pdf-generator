import { combineReducers } from "redux";
import inputs from "./inputs.reducers";
import table from "./table.reducers";

const reducers = combineReducers({
  inputs,
  table,
});

export default reducers;

// needed for useSelector
export type RootState = ReturnType<typeof reducers>;
