import { Action, Types } from "../actions/inputs.actions";
import { customerInputs } from "constants/inputs/customer.inputs";
import { invoiceInputs } from "constants/inputs/invoice.inputs";
import { companyInputs } from "constants/inputs/company.inputs";

export interface DataInput {
  placeholder: string;
  key: string;
  value: string;
  label?: string;
}

interface State {
  customer: DataInput[];
  invoice: DataInput[];
  company: DataInput[];
}

const initialState: State = {
  customer: customerInputs,
  invoice: invoiceInputs,
  company: companyInputs,
};

const reducer = (state: State = initialState, action: Action): State => {
  switch (action.type) {
    case Types.UPDATE_INPUT:
      const { index, eventTargetValue, stateToUpdate } = action.payload;
      const newStateKey = [...state[stateToUpdate]];
      newStateKey[index].value = eventTargetValue;

      return { ...state, [`${stateToUpdate}`]: newStateKey };

    default:
      return state;
  }
};

export default reducer;
