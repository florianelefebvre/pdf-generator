import { TableAction, TypesTable } from "../actions/table.actions";

import { tableHeadingsInputs } from "constants/inputs/tableHeadings.inputs";
import { tableBlankRowInputs } from "constants/inputs/tableBlankRow.inputs";
import { swap } from "lib/swap";
import { totalRows } from "constants/inputs/totalsRows.inputs";

export interface Row {
  subtotal: boolean;
  subtitle: boolean;
  columns: {
    key: string;
    placeholder?: string;
    value: string;
    subtitle?: {
      placeholder?: string;
      value?: string;
    };
    subtotal?: {
      value?: string;
    };
  }[];
}

interface Table {
  deposit: boolean;
  headings: Row;
  rows: Row[];
  totalRows: {
    label: string;
    value: string;
  }[];
  totalVatRows: {
    label: string;
    value: string;
  }[];
}

const initialState: Table = {
  deposit: true,
  headings: tableHeadingsInputs,
  rows: [tableBlankRowInputs, tableBlankRowInputs, tableBlankRowInputs],
  totalRows: totalRows,
  totalVatRows: [{ label: "", value: "" }],
};

const reducer = (state: Table = initialState, action: TableAction): Table => {
  switch (action.type) {
    case TypesTable.ADD_BLANK_ROW:
      const { rowIndex: rowIndex6 } = action.payload;

      const newTable6 = [...state.rows];
      newTable6.splice(rowIndex6 + 1, 0, tableBlankRowInputs);

      return {
        ...state,
        rows: newTable6,
      };

    case TypesTable.SET_CHECKBOX:
      const {
        checkBoxId,
        eventTargetValue: eventTargetValue3,
        rowIndex: rowIndex3,
      } = action.payload;
      const newRow3 = { ...state.rows[rowIndex3] };
      switch (checkBoxId) {
        case "=":
          newRow3.subtotal = eventTargetValue3;
          break;
        case "T":
          newRow3.subtitle = eventTargetValue3;
          break;
      }
      const newTable3 = [...state.rows];
      newTable3.splice(rowIndex3, 1, newRow3);

      return {
        ...state,
        rows: newTable3,
      };

    case TypesTable.DELETE_ROW:
      const { rowIndex: rowIndex5 } = action.payload;

      const newTable5 = [...state.rows];
      newTable5.splice(rowIndex5, 1);

      return {
        ...state,
        rows: newTable5,
      };

    case TypesTable.SWAP_ROW:
      const { direction, rowIndex: rowIndex4 } = action.payload;
      let newIndex;
      if (direction === "up") newIndex = rowIndex4 - 1;
      else newIndex = rowIndex4 + 1;

      const newTable4 = swap([...state.rows], newIndex, rowIndex4);

      return {
        ...state,
        rows: newTable4,
      };

    case TypesTable.UPDATE_INPUT_TABLE:
      const { columnIndex, eventTargetValue, inputType, rowIndex } =
        action.payload;

      // update subtotal
      let newSubtotal;
      if (inputType === "subtotal") {
        newSubtotal = {
          ...state.rows[rowIndex].columns[columnIndex].subtotal,
          value: eventTargetValue,
        };
      }

      // update subtitle
      let newSubtitle;
      if (inputType === "subtitle") {
        newSubtitle = {
          ...state.rows[rowIndex].columns[columnIndex].subtitle,
          value: eventTargetValue,
        };
      }

      // update cell
      const newCell = {
        ...state.rows[rowIndex].columns[columnIndex],
      };

      switch (inputType) {
        case "subtitle":
          newCell.subtitle = newSubtitle;
          break;
        case "subtotal":
          newCell.subtotal = newSubtotal;
          break;
        case "value":
          newCell.value = eventTargetValue;
          break;
      }

      // update columns
      const newColumns = [...state.rows[rowIndex].columns];
      newColumns.splice(columnIndex, 1, newCell);

      // update row
      const newRow = { ...state.rows[rowIndex], columns: newColumns };

      // update Table
      const newTable = [...state.rows];
      newTable.splice(rowIndex, 1, newRow);

      return {
        ...state,
        rows: newTable,
      };

    case TypesTable.UPDATE_TOTALS:
      const { rowIndex: rowIndex1, value } = action.payload;

      // update value
      const newTotalRowCell = {
        ...state.totalRows[rowIndex1],
        value,
      };
      const newTotalRows = [...state.totalRows];
      newTotalRows.splice(rowIndex1, 1, newTotalRowCell);
      return {
        ...state,
        totalRows: newTotalRows,
      };

    case TypesTable.UPDATE_VAT_TOTALS:
      const { vatTotalRows } = action.payload;
      return {
        ...state,
        totalVatRows: vatTotalRows,
      };

    default:
      return state;
  }
};

export default reducer;
