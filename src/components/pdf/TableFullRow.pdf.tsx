import { Text, View, StyleSheet } from "@react-pdf/renderer";
import { FunctionComponent } from "react";
import { Row } from "redux/reducers/table.reducers";

type TableFullRowProps = {
  row: Row;
  borderWidth?: string;
  rowHeading?: boolean;
};

const styles = StyleSheet.create({
  row: {
    display: "flex",
    flexDirection: "row",
  },
});

// voir après car une colonne en trop par rapport au headings
const flexSizeColumns = [2, 6, 2, 0, 2, 3, 2];

const TableFullRow: FunctionComponent<TableFullRowProps> = ({
  row,
  rowHeading,
}) => {
  return (
    <View style={styles.row}>
      {row.columns.map((cell, index) => {
        let alignItems = "center";
        let cellBorderTopWidth = "0px";
        let rowBorderTopWidth = "0px";
        let cellBorderLeftWidth = "0px";

        if (cell.key === "name") {
          alignItems = "flex-start";
          cellBorderTopWidth = "1px";
        }
        if (cell.key === "subtotalPriceET") cellBorderTopWidth = "1px";
        if (rowHeading === true) rowBorderTopWidth = "1px";
        if (index === 0) cellBorderLeftWidth = "1px";

        return (
          cell.key !== "discountPercent" && (
            <View
              key={index}
              // debug={true}
              style={{
                flex: 1,
                borderColor: "black",
                borderWidth: "1px",
                borderTopWidth: rowBorderTopWidth,
                borderLeftWidth: cellBorderLeftWidth,
              }}
            >
              {row.subtitle && (
                <View
                  // debug={true}
                  style={{
                    minHeight: "25px",
                    alignItems,
                    padding: "2px",
                    paddingTop: "10px",
                  }}
                >
                  {cell.key === "name" && <Text>{cell.subtitle?.value}</Text>}
                </View>
              )}

              <View
                style={{
                  minHeight: "20px",
                  alignItems,
                  padding: "2px",
                }}
                // debug={true}
              >
                <Text>{cell.value}</Text>
              </View>

              {row.subtotal && (
                <View
                  // debug={true}
                  style={{
                    minHeight: "25px",
                    borderTopWidth: cellBorderTopWidth,
                    padding: "2px",
                    borderTopColor: "black",
                    display: "flex",
                    flexDirection: "column",
                    alignItems,
                    paddingBottom: "10px",
                  }}
                >
                  {cell.key === "name" && <Text>Sous-total</Text>}
                  {cell.key === "subtotalPriceET" && (
                    <Text>{cell.subtotal?.value}</Text>
                  )}
                </View>
              )}
            </View>
          )
        );
      })}
    </View>
  );
};

export default TableFullRow;
