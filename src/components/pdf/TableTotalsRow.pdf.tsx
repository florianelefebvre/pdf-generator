import { Text, View, StyleSheet } from "@react-pdf/renderer";
import { FunctionComponent } from "react";

type TableTotalsRowProps = {
  row: {
    label: string;
    value: string;
  };
};

const styles = StyleSheet.create({
  row: {
    display: "flex",
    flexDirection: "row",
  },
  name: {
    textAlign: "right",
    padding: "5px",
  },
  value: {
    textAlign: "center",
    padding: "5px",
  },
});

const TableTotalsRow: FunctionComponent<TableTotalsRowProps> = ({ row }) => {
  const flexSize = [4, 1, 1];
  return (
    <View style={styles.row}>
      <View style={{ flex: flexSize[0] }}>
        <Text style={styles.name}>{row.label}</Text>
      </View>

      <View
        style={{
          flex: flexSize[1],
          borderWidth: "1px",
          borderColor: "black",
          borderTopWidth: "0px",
        }}
      >
        <Text style={styles.value}>{row.value}</Text>
      </View>

      <View style={{ flex: flexSize[2] }} />
    </View>
  );
};

export default TableTotalsRow;
