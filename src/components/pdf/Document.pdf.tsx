import { FunctionComponent } from "react";

import {
  Document,
  Page,
  Text,
  View,
  StyleSheet,
  Image,
} from "@react-pdf/renderer";

// data
import TableFullRow from "./TableFullRow.pdf";
import TableTotalsRow from "./TableTotalsRow.pdf";

// constants
import { tableHeadings } from "constants/pdf/tableHeadings.pdf";

// interfaces
import { pdfDataObj } from "interfaces/data.pdf";

type PdfDocumentProps = {
  data: pdfDataObj;
};

const styles = StyleSheet.create({
  container: {
    width: "90%",
    height: "90%",
    margin: "auto",
  },
  page: {
    fontSize: "10px",
  },

  view: {
    lineHeight: "3px",
  },

  logo: {
    color: "black",
    fontSize: "20px",
    display: "flex",
    flexDirection: "row",
  },

  header: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    marginBottom: "40px",
  },
});

const PdfDocument: FunctionComponent<PdfDocumentProps> = ({ data }) => {
  const {
    header: { companyInputsData, customerInputsData, invoiceInputsData },
    rows,
    totalRows,
    totalVatRows,
  } = data;
  console.log("ICIIII", totalRows[0]);
  return (
    <Document>
      <Page style={styles.page}>
        <View style={styles.container}>
          {/* header */}
          <View style={(styles.view, styles.header)}>
            {/* left */}
            <View style={(styles.view, { lineHeight: "1.5px", width: "60%" })}>
              <View style={styles.logo}>
                {/* eslint-disable-next-line jsx-a11y/alt-text */}
                <Image
                  src="logo_big_black.png"
                  style={{ height: "20px", width: "20px" }}
                />
                <Text style={{ marginLeft: "10px", marginBottom: "22px" }}>
                  USER AGENCY
                </Text>
              </View>
              {companyInputsData.map((input, index) => {
                if (input.value)
                  return (
                    <Text key={index}>
                      {input.label} {input.value}
                    </Text>
                  );
              })}
            </View>

            {/* right */}
            <View style={(styles.view, { width: "40%" })}>
              <View
                style={{
                  borderLeftColor: "black",
                  borderLeftWidth: "1px",
                  padding: "5px",
                  lineHeight: "1.5px",
                }}
              >
                <Text>
                  {invoiceInputsData[0].label} {invoiceInputsData[0].value}
                </Text>
                <Text>
                  {invoiceInputsData[1].label} {invoiceInputsData[1].value}
                </Text>
              </View>

              <View
                style={{
                  borderRadius: "8px",
                  borderWidth: "1px",
                  padding: "5px",
                  lineHeight: "1.5px",
                  marginTop: "10px",
                }}
              >
                {customerInputsData.map((input, index) => {
                  if (input.value)
                    return (
                      <Text key={index}>
                        {input.label} {input.value}
                      </Text>
                    );
                })}
              </View>

              <View
                style={{
                  padding: "5px",
                  lineHeight: "1.5px",
                  marginTop: "10px",
                }}
              >
                <Text style={{ fontSize: "15px" }}>
                  {invoiceInputsData[2].value}
                </Text>
              </View>
            </View>
          </View>

          <View>
            <TableFullRow
              row={tableHeadings}
              borderWidth="1px"
              rowHeading={true}
            />
            {rows.map((row, index) => {
              return <TableFullRow key={index} row={row} />;
            })}

            <TableTotalsRow row={totalRows[0]} />
            {totalVatRows[0].value !== "" &&
              totalVatRows.map((row, index) => {
                return <TableTotalsRow key={index} row={row} />;
              })}

            {totalRows.map((row, index) => {
              if (index > 0) return <TableTotalsRow key={index} row={row} />;
            })}
          </View>
        </View>
      </Page>
    </Document>
  );
};

export default PdfDocument;
