import { Input, Text } from "@geist-ui/core";
import { FunctionComponent } from "react";
import { useDispatch } from "react-redux";
import { updateDeposit, updateTotals } from "redux/actions/table.actions";
import styled from "styled-components";

type TableTotalsRowProps = {
  row: {
    value: string;
    label: string;
  };
};

const TableTotalsRow: FunctionComponent<TableTotalsRowProps> = ({ row }) => {
  const array = [0, 1, 2, 3, 4, 5, 6, 7];

  const dispatch = useDispatch();
  return (
    <Row>
      {array.map((column, index) => {
        console.log(row.label);
        return (
          <Cell key={index}>
            {index === 4 && (
              <Label>
                <Text
                  font="12px"
                  style={{
                    margin: 0,
                    padding: "2px",
                    textAlign: "right",
                  }}
                >
                  {row.label}
                </Text>
              </Label>
            )}
            {index === 5 && (
              <Total>
                {row.label === "Acompte" ? (
                  <Input
                    font="12px"
                    height={0.5}
                    value={row.value}
                    style={{ textAlign: "right", padding: "0", margin: "0" }}
                    onChange={(event) => {
                      dispatch(
                        updateDeposit({
                          rowIndex: 2,
                          value: event.target.value,
                        })
                      );
                    }}
                  />
                ) : (
                  <Text
                    font="12px"
                    style={{
                      margin: 0,
                      padding: "2px",
                      textAlign: "right",
                    }}
                  >
                    {row.value}
                  </Text>
                )}
              </Total>
            )}
          </Cell>
        );
      })}
    </Row>
  );
};

export default TableTotalsRow;

const Cell = styled.div``;

const Row = styled.div`
  display: grid;
  flex-direction: row;
  grid-template-columns: 2.5fr 5fr 1.8fr 1.5fr 2.5fr 2.2fr 2fr 1.5fr;
`;

const Label = styled.div``;

const Total = styled.div`
  border: solid black 1px;
  border-top: none;
`;
