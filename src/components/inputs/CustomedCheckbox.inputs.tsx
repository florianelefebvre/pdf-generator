// components
import { Text, Checkbox } from "@geist-ui/core";

type CustomedCheckboxProps = {
  onChange: (eventTargetValue: boolean) => void;
  label: string;
  checked: boolean;
  color: string;
};
const CustomedCheckbox = ({
  onChange,
  label,
  checked,
  color,
}: CustomedCheckboxProps) => {
  return (
    <>
      <Checkbox
        font="10px"
        checked={checked}
        onChange={(event) => onChange(event.target.checked)}
      >
        <Text
          font="10px"
          style={{
            color,
            fontWeight: "bold",
          }}
        >
          {label}
        </Text>
      </Checkbox>
    </>
  );
};

export default CustomedCheckbox;
