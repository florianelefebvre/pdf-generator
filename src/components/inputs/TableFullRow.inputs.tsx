import { FunctionComponent } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

// components
import { Text, Input } from "@geist-ui/core";

// icons
import { DownArrow, UpArrow } from "@styled-icons/boxicons-solid";
import { Cross } from "@styled-icons/entypo";
import { Plus } from "@styled-icons/boxicons-regular";
import {
  addBlankRow,
  deleteRow,
  updateCheckboxes,
  updateRow,
  UpdateTableOrder,
} from "redux/actions/table.actions";
import CustomedCheckbox from "components/inputs/CustomedCheckbox.inputs";
import { Row } from "redux/reducers/table.reducers";

type TableFullRowInputsProps = {
  row: Row;
  rowHeading?: boolean;
  index: number;
  handleCheckboxes?: any;
  lastRow?: boolean;
};

const TableFullRowInputs: FunctionComponent<TableFullRowInputsProps> = ({
  row,
  rowHeading,
  index,
  lastRow,
}) => {
  const dispatch = useDispatch();

  return (
    <StyledRow>
      {row.columns.map((cell, indexColumn) => {
        return (
          <Cell key={indexColumn} index={indexColumn} rowHeading={rowHeading}>
            {row.subtitle && (
              <Subtitle>
                {cell.key === "name" && (
                  <Input
                    font="10px"
                    height={0.5}
                    value={cell.subtitle?.value}
                    placeholder={cell.subtitle?.placeholder}
                    onChange={(event) => {
                      dispatch(
                        updateRow({
                          rowIndex: index,
                          columnIndex: indexColumn,
                          eventTargetValue: event.target.value,
                          inputType: "subtitle",
                        })
                      );
                    }}
                  />
                )}
              </Subtitle>
            )}

            <Value>
              {rowHeading ? (
                <Text margin={0} font="10px">
                  {cell.value}
                </Text>
              ) : (
                <Input
                  disabled={cell.key === "subtotalPriceET"}
                  font="10px"
                  width="100%"
                  height={0.5}
                  placeholder={cell.placeholder}
                  value={cell.value}
                  onChange={(event) => {
                    dispatch(
                      updateRow({
                        rowIndex: index,
                        columnIndex: indexColumn,
                        eventTargetValue: event.target.value,
                        inputType: "value",
                      })
                    );
                  }}
                />
              )}
            </Value>

            {row.subtotal && (
              <Subtotal>
                {cell.key === "name" && (
                  <Text
                    font="10px"
                    style={{
                      borderTop: "solid 1px grey",
                      paddingRight: "10px",
                      paddingTop: "2px",
                      textAlign: "right",
                    }}
                  >
                    Sous-total
                  </Text>
                )}
                {cell.key === "subtotalPriceET" && (
                  <Text
                    font="10px"
                    style={{
                      borderTop: "solid 1px grey",
                      paddingRight: "10px",
                      paddingTop: "2px",
                      textAlign: "right",
                    }}
                  >
                    {cell.subtotal?.value}
                  </Text>
                )}
              </Subtotal>
            )}
          </Cell>
        );
      })}
      {!rowHeading && (
        <Actions>
          <FlexCol>
            <CustomedCheckbox
              label="T"
              color="blue"
              checked={row.subtitle}
              onChange={(eventTargetValue) => {
                dispatch(
                  updateCheckboxes({
                    rowIndex: index,
                    checkBoxId: "T",
                    eventTargetValue,
                  })
                );
              }}
            />
            <CustomedCheckbox
              label="="
              checked={row.subtotal}
              color="#c65102"
              onChange={(eventTargetValue) => {
                dispatch(
                  updateCheckboxes({
                    rowIndex: index,
                    checkBoxId: "=",
                    eventTargetValue,
                  })
                );
              }}
            />
          </FlexCol>
          <FlexCol>
            {index !== 0 && (
              <UpArrow
                color="orange"
                size="10px"
                onClick={() =>
                  dispatch(
                    UpdateTableOrder({ direction: "up", rowIndex: index })
                  )
                }
                style={{ cursor: "pointer" }}
              />
            )}

            {!lastRow && (
              <DownArrow
                color="orange"
                size="10px"
                onClick={() =>
                  dispatch(
                    UpdateTableOrder({ direction: "down", rowIndex: index })
                  )
                }
                style={{ cursor: "pointer" }}
              />
            )}
          </FlexCol>

          <FlexCol>
            <Cross
              color="red"
              size="12px"
              style={{ cursor: "pointer" }}
              onClick={() => dispatch(deleteRow({ rowIndex: index }))}
            />
            <Plus
              color="green"
              size="12px"
              onClick={() => dispatch(addBlankRow({ rowIndex: index }))}
              style={{ cursor: "pointer" }}
            />
          </FlexCol>
        </Actions>
      )}
    </StyledRow>
  );
};

export default TableFullRowInputs;

interface CellProps {
  readonly index: number;
  readonly rowHeading?: boolean;
}

const StyledRow = styled.div`
  display: grid;
  flex-direction: row;
  grid-template-columns: 2.5fr 5fr 1.8fr 1.5fr 2.5fr 2.2fr 2fr 1.5fr;
`;

const Cell = styled.div<CellProps>`
  padding: 2px;
  border-right: solid black 1px;
  border-bottom: solid black 1px;
  border-left: ${({ index }) => index === 0 && "solid black 1px"};

  // rowHeading
  border-top: ${({ rowHeading }) => rowHeading && "solid black 1px"};
  background-color: ${({ rowHeading }) => rowHeading && "#6CB5FF"};
  padding: ${({ rowHeading }) => rowHeading && "10px 5px 0 5px"};
`;

const Subtitle = styled.div`
  min-height: 25px;
`;

const Value = styled.div`
  min-height: 25px;
`;

const Subtotal = styled.div`
  min-height: 25px;
`;

const Actions = styled.div`
  background-color: #fff7f7;
  border: solid 1px #ebe3e3;
  display: flex;
  flex-direction: row;
`;

const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 3px;
`;
